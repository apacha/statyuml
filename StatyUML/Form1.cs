﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.IO.Compression;
using System.Drawing.Imaging;

namespace StatyUML
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string encode64(byte[] compressedString)
        {
            var str = "";
            var len = compressedString.Length;
            for (int i = 0; i < len; i+=3)
            {
                if (i + 2 == len)
                {
                    str += encode3bytes(compressedString[i], compressedString[i + 1], 0);
                } else if (i+1 == len)
                {
                    str += encode3bytes(compressedString[i], 0, 0);
                } else
                {
                    str +=encode3bytes(compressedString[i], compressedString[i + 1], compressedString[i + 2]);
                }
            }

            return str;
        }

        private string encode3bytes(int b1, int b2, int b3)
        {
            var c1 = b1 >> 2;
            var c2 = ((b1 & 0x3) << 4) | (b2 >> 4);
            var c3 = ((b2 & 0xF) << 2) | (b3 >> 6);
            var c4 = b3 & 0x3F;
            var res = "";
            res += encode6bit(Convert.ToByte(c1 & 0x3F));
            res += encode6bit(Convert.ToByte(c2 & 0x3F));
            res += encode6bit(Convert.ToByte(c3 & 0x3F));
            res += encode6bit(Convert.ToByte(c4 & 0x3F));
            return res;
        }

        private char encode6bit(byte b)
        {
            if (b <10)
            {
                return Convert.ToChar(48 + b);
            }
            b -= 10;
            if (b < 26)
            {
                return Convert.ToChar(65 + b);
            }
            b -= 26;
            if (b < 26)
            {
                return Convert.ToChar(97 + b);
            }
            b -= 26;
            if (b == 0)
            {
                return '-';
            }      
            if (b == 1)
            {
                return '_';
            }
            return '?';
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select Config File";
            fdlg.InitialDirectory = @"c:\";
            fdlg.Filter = "CS File (*.cs)|*.cs";

            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                string file = fdlg.FileName;
                try
                {
                    string text = File.ReadAllText(file);
                    string[] seperators = { ".WhenIn(" , ".When("};
                    var words = text.Split(seperators, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()).ToList();

                    var transitions = words.Skip(1).Where(s => !s.Contains(".IgnoreEvent()") && !s.Contains(".ThrowInvalidTransitionException()")).ToList();

                    string[] delimiters = { ").AndReceived(", ").HappensIn(States.", ").GoTo(", ")", "States.", "Events."};

                    List<String> result = new List<string>();
                    result.Add("@startuml");
                    var start = 1;
                    var oldVersion = true;

                    foreach (string s in transitions)
                    {                 
                        string[] StatesAndTransitions = s.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (start == 1)
                        {
                            if (s.Contains(").AndReceived("))
                            {
                                oldVersion = false;
                                result.Add("[*] --> " + StatesAndTransitions[0]);
                            }
                            else
                            {
                                result.Add("[*] --> " + StatesAndTransitions[1]);
                            }
                            start++;
                        }
                        if (oldVersion)
                        {
                            result.Add(StatesAndTransitions[1] + " --> " + StatesAndTransitions[2] + " : " + StatesAndTransitions[0]);
                        } else
                        {
                            result.Add(StatesAndTransitions[0] + " --> " + StatesAndTransitions[2] + " : " + StatesAndTransitions[1]);
                        }     
                    }

                    result.Add("@enduml");
                    var plantUmlText = String.Join("\n", result);

                    //Encoding for PlantUML server
                    //Encoded in UTF8 --> Compressed using Deflate algorithm --> Reencoded in ASCII using a transformation close to base64
                    byte[] bytesToEncode = UTF8Encoding.UTF8.GetBytes(plantUmlText);
                    MemoryStream resultStream = new MemoryStream();
                    DeflateStream zip = new DeflateStream(resultStream, CompressionMode.Compress);
                    zip.Write(bytesToEncode, 0, bytesToEncode.Length);
                    zip.Close();
                    byte[] compressed = resultStream.ToArray();
                    string encodedResult = encode64(compressed);

                    HttpWebRequest request = HttpWebRequest.CreateHttp("http://www.plantuml.com/plantuml/img/"+encodedResult);
                    request.Method = "GET";
                    request.ContentType = "text/xml; charset=x-user-defined";

                    WebResponse response = (HttpWebResponse)request.GetResponse();

                    using (Stream stream = response.GetResponseStream())
                    {
                        var img = Image.FromStream(stream);
                        pictureBox1.Image = img;
                        img.Save(file.Replace(".cs", ".png"), ImageFormat.Png);
                    }
                }
                catch (IOException)
                {
                }
            }
        }
    }
}
