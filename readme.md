# StatyUML - A smart documentation tool for Staty #

Staty is a smart state-machine for .NET. Written as C# portable class library (PCL), Staty comes as an expressive event-driven state-machine. Staty has influences from other state machine frameworks, particularly by [David Lafreniere's C++ State Machine](http://www.drdobbs.com/cpp/state-machine-design-in-c/184401236). 
StatyUML can be used to automatically generate UML diagrams from existing Staty configurations.

[![Build status](https://ci.appveyor.com/api/projects/status/m0tje6og2u1qmlve?svg=true)](https://ci.appveyor.com/project/apacha/statyuml)

## Features ##

- Import existing cs-files
- Generate UML diagrams of existing configurations

## Contributing ##

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## License & Copyright ##
Released under the MIT license.

Copyright, 2016, by Ichrak Ketata and [Alexander Pacha](http://my-it.at).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.